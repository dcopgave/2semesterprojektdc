package dao;

import model.Ramp;
import model.TransportOrder;
import model.Truck;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by H&J Group on 25-03-2015.
 */
public class Storage {
    private static ArrayList<Truck> trucks = new ArrayList<>();
    private static ArrayList<Ramp> ramps = new ArrayList<>();
    private static ArrayList<TransportOrder> transportOrders = new ArrayList<>();


    public static ArrayList<Truck> getTrucks(){
        return new ArrayList<>(trucks);
    }

    public static void addTruck(Truck truck){
        trucks.add(truck);
    }

    public static void addRamp(Ramp ramp){
        ramps.add(ramp);
    }

    public static ArrayList<Ramp> getRamps(){
        return new ArrayList<>(ramps);
    }

    public static void addTransportOder(TransportOrder transportOrder){
        transportOrders.add(transportOrder);
    }

    public static ArrayList<TransportOrder> getTransportOrders(){
        return new ArrayList<>(transportOrders);
    }

}
