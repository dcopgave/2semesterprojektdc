package model;

import java.time.Duration;
import java.time.LocalDate;

/**
 * Created by H&J Group on 25-03-2015.
 */
public class TransportPartialOrder {
    private double weight;
    private LocalDate loadingDate;
    private Duration loadingDuration;
    private String id;
    private Truck truck;
    private boolean isDone;
    private TransportOrder transportOrder;

    public TransportPartialOrder(double weight, LocalDate loadingDate, Duration loadingDuration, String id,
                                 Truck truck, TransportOrder transportOrder) {
        this.weight = weight;
        this.loadingDate = loadingDate;
        this.loadingDuration = loadingDuration;
        this.id = id;
        this.transportOrder = transportOrder;
        this.truck = truck;
        this.isDone = false;
        truck.addPartialOrder(this);
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public LocalDate getLoadingDate() {
        return loadingDate;
    }

    public void setLoadingDate(LocalDate loadingDate) {
        this.loadingDate = loadingDate;
    }

    public Duration getLoadingDuration() {
        return loadingDuration;
    }

    public void setLoadingDuration(Duration loadingDuration) {
        this.loadingDuration = loadingDuration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Truck getTruck() {
        return truck;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean isDone) {
        this.isDone = isDone;
    }

    public TransportOrder getTransportOrder() {
        return transportOrder;
    }

    public void setTransportOrder(TransportOrder transportOrder) {
        this.transportOrder = transportOrder;
    }

    @Override
    public String toString() {
        return "Transportdelordre nr. " + this.getId() + "\n \t" +
                "Lastbil: nr = " + truck.getId() +", " + truck.getTruckdriver().getName() +  " \n \t" +
                "Læssetid: " + loadingDuration.toMinutes() + " min." +"\n \t" +
                "Vægt: " + weight + " kg" + "\n \t" +
                "Læssedato: " + loadingDate + "\n";
    }
}
