package model;

import model.enumClasses.OrderStatus;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Created by H&J Group on 25-03-2015.
 */
public class LoadingOrder {
    private OrderStatus status;
    private LocalTime expectedStart;
    private LocalTime expectedEnd;
    private LocalTime actualStart;
    private LocalTime actualEnd;
    private Ramp ramp;
    private Truck truck;
    private ErrorQueue errorQueue = ErrorQueue.getInstance();
    private LocalDate loadingDate;

    public LoadingOrder(Truck truck) {
        this.truck = truck;
        this.status = OrderStatus.QUEUED;
        this.loadingDate = LocalDate.now();
    }

    public OrderStatus getStatus() {
        return status;
    }

    /**
     * sets the status for the LoadingOrder.
     * If the status is ERROR then the method removes it from the ramps queue and adds it to the ErrorQueue.
     * @param boolean status
     */
    public void setStatus(OrderStatus status) {
        this.status = status;
        if(status.equals(OrderStatus.ERROR)) {
            this.errorQueue.addToQueue(this);
        }

        if(status.equals(OrderStatus.LOADED)) {
            this.ramp.getQueue().remove(this);
        }
        //TODO : Tilføj til statistik klasse.
    }

    public LocalTime getExpectedStart() {
        return expectedStart;
    }

    public void setExpectedStart(LocalTime expectedStart) {
        this.expectedStart = expectedStart;
    }

    public LocalTime getExpectedEnd() {
        return expectedEnd;
    }

    public void setExpectedEnd(LocalTime expectedEnd) {
        this.expectedEnd = expectedEnd;
    }

    public LocalTime getActualStart() {
        return actualStart;
    }

    public void setActualStart(LocalTime actualStart) {
        this.actualStart = actualStart;
    }

    public LocalTime getActualEnd() {
        return actualEnd;
    }

    public void setActualEnd(LocalTime actualEnd) {
        this.actualEnd = actualEnd;
    }

    public Ramp getRamp() {
        return ramp;
    }

    public void setRamp(Ramp ramp) {
        this.ramp = ramp;
    }

    public Truck getTruck() {
        return truck;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    public LocalDate getLoadingDate() {
        return loadingDate;
    }

    public void setLoadingDate(LocalDate loadingDate) {
        this.loadingDate = loadingDate;
    }

    /**
     *
     * @return the loadingorder to a string that matches our gui
     */
    @Override
    public String toString() {
        String startHour = getExpectedStart().getHour() + "";
        String startMinute = getExpectedStart().getMinute() + "";

        String endHour = getExpectedEnd().getHour() + "";
        String endMinute = getExpectedEnd().getMinute() + "";

        if (startHour.length() < 2){
            startHour = "0" + startHour;
            if(startHour.equals("0")) {
                startHour += "0";
            }
        }
        if (startMinute.length() < 2){
            startMinute = "0" + startMinute;
            if(startMinute.equals("0")) {
                startMinute += "0";
            }
        }
        if (endHour.length() < 2){
            endHour = "0" + endHour;
            if(endHour.equals("0")) {
                endHour += "0";
            }
        }
        if (endMinute.length() < 2){
            endMinute = "0" + endMinute;
            if(endMinute.equals("0")) {
                endMinute += "0";
            }
        }


        return "Her er din læsseinformation: \n \t" +
                "Lastbilens id: " + getTruck().getId() + " \n \t" +
                "Forventet starttidspunkt på læsningen: " + startHour + ":" + startMinute + "\n \t" +
                "Forventet sluttidspunkt på læsningen: " + endHour + ":" + endMinute + "\n \t" +
                "Lastens totalvægt: " + truck.getTotalLoadingWeight() + " kg";
    }


}
