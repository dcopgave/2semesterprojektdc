package model;

import java.time.Duration;
import java.time.LocalTime;

/**
 * Created by H&J Group on 25-03-2015.
 */
public class Truckdriver {
    private String name;
    private String phonenumber;
    private Duration resttime;
    private Truck truck;

    public Truckdriver(String name, Truck truck) {
        this.name = name;
        this.truck = truck;
    }
    public void setAdditionalInformation(String phonenumber, Duration resttime) {
        this.phonenumber = phonenumber;
        this.resttime = resttime;
    }

    /**
     *
     * @return the time for the ealiest departure for the truckdriver.
     */
    public LocalTime getEarliestDepartureTime() {
        return LocalTime.now().plus(resttime);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Duration getResttime() {
        return resttime;
    }

    public void setResttime(Duration resttime) {
        this.resttime = resttime;
    }

    public Truck getTruck() {
        return truck;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }
}
