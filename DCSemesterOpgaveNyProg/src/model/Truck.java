package model;
import model.enumClasses.OrderStatus;
import model.enumClasses.Transportmateriel;
import model.enumClasses.TruckStatus;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
/**
 * Created by H&J Group on 25-03-2015.
 */
public class Truck {
    private double arrivalWeight;
    private double departureWeight;
    private String id;
    private ArrayList<TransportPartialOrder> partialOrders = new ArrayList<>();
    private ArrayList<LoadingOrder> loadingOrders = new ArrayList<>();
    private Truckdriver truckdriver;
    private TruckStatus status;

    public Truck(String id, String name) {
        this.id = id;
        this.truckdriver = new Truckdriver(name, this);
        this.status = TruckStatus.NOT_ARRIVED;
    }
    /**
     * Get the lowest weight margin and the sum of the weights of the orders
     * Only get orders that are still being processed
     * @param aDepartureWeight
     * @return
     */
    public boolean checkDepartureWeight(double aDepartureWeight) {
        boolean result = false;
        double weightChange = aDepartureWeight - arrivalWeight;
        double lowestMargin = 100;
        double ordersWeight = 0;
        for(TransportPartialOrder po : getPartialOrders()) {
            if(!po.isDone() && po.getLoadingDate().equals(LocalDate.now())) {
                ordersWeight += po.getWeight();
                TransportOrder to = po.getTransportOrder();
                if(to.getWeightMargin() < lowestMargin) {
                    lowestMargin = to.getWeightMargin();
                }
            }
        }
        double marginInDecimal = lowestMargin/100;
        if(weightChange <= (ordersWeight * (1 + marginInDecimal)) &&
                weightChange > ordersWeight * (1 - marginInDecimal)) {
            result = true;
        } else {
            for(LoadingOrder lo : getLoadingOrders()) {
                if(!lo.getStatus().equals(OrderStatus.DONE)) {
                    lo.setStatus(OrderStatus.ERROR);
                }
            }
        }
        return result;
    }

    /**
     * Finds the totalLoadingTime for the trucks loadingOrder for this day.
     * @return
     * @throws RuntimeException
     */
    public Duration getTotalLoadingTime() throws RuntimeException{
        Duration duration = Duration.ofMinutes(0);

        for(TransportPartialOrder t : this.getPartialOrders()) {
            if(!t.isDone() && t.getLoadingDate().equals(LocalDate.now())) {
                Duration newDuration = duration.plusMinutes(t.getLoadingDuration().toMinutes());
                duration = newDuration;
            }
        }
        if(!duration.equals(Duration.ofMinutes(0))) {
            return duration;
        } else {
            throw new RuntimeException("Truck doesn't have any undone TransportPartialOrders");
        }
    }

    public ArrayList<TransportPartialOrder> getTodaysPartialOrders(){
        ArrayList<TransportPartialOrder> transportPartialOrders = new ArrayList<>();
        for(TransportPartialOrder t : this.getPartialOrders()) {
            if(!t.isDone() && t.getLoadingDate().equals(LocalDate.now())) {
                transportPartialOrders.add(t);
            }
        }
        return new ArrayList<>(transportPartialOrders);
    }

    public Transportmateriel getTransportmateriel(){
        for(TransportPartialOrder t : this.getPartialOrders()) {
            if(!t.isDone() && t.getLoadingDate().equals(LocalDate.now())) {
                return t.getTransportOrder().getTransportmateriel();
            }
        }
        return null;
    }

    /**
     *
     * @return the total weight of all the partialorders for the truck that are today.
     */
    public int getTotalLoadingWeight(){
        int weight = 0;
        for (TransportPartialOrder t : getTodaysPartialOrders()){
            weight += t.getWeight();
        }
        return weight;
    }

    /**
     *
     * @return the loadingorder for today, and that are not done.
     */
    public LoadingOrder getTodaysLoadingOrder() {
        for(LoadingOrder l : getLoadingOrders()) {
            if(!l.getStatus().equals(OrderStatus.DONE) && l.getLoadingDate().equals(LocalDate.now()) ) {
                return l;
            }
        }
        return null;
    }

    public double getArrivalWeight() {
        return arrivalWeight;
    }

    public void setArrivalWeight(double arrivalWeight) {
        this.arrivalWeight = arrivalWeight;
    }

    public double getDepartureWeight() {
        return departureWeight;
    }

    public void setDepartureWeight(double departureWeight) {
        this.departureWeight = departureWeight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<TransportPartialOrder> getPartialOrders() {
        return new ArrayList<>(partialOrders);
    }

    public void addPartialOrder(TransportPartialOrder partialOrder) {
        this.partialOrders.add(partialOrder);
    }

    public ArrayList<LoadingOrder> getLoadingOrders() {
        return new ArrayList<>(loadingOrders);
    }

    public void addLoadingOrder(LoadingOrder loadingOrder) {
        this.loadingOrders.add(loadingOrder);
    }

    public Truckdriver getTruckdriver() {
        return truckdriver;
    }

    public void setTruckdriver(Truckdriver truckdriver) {
        this.truckdriver = truckdriver;
    }

    public TruckStatus getStatus() {
        return status;
    }

    public void setStatus(TruckStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "arrivalWeight=" + arrivalWeight +
                ", departureWeight=" + departureWeight +
                ", id='" + id + '\'' +
                ", status=" + status +
                '}';
    }
}
