package model.enumClasses;

/**
 * Created by H&J Group on 25-03-2015.
 */
public enum OrderStatus {
    QUEUED,
    ONGOING,
    LOADED,
    DONE,
    ERROR
}
