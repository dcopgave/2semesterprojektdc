package model.enumClasses;
/**
 * Created by H&J Group on 25-03-2015.
 */
public enum TruckStatus {
    ARRIVED,
    LOADING,
    LOADED,
    DEPARTURED,
    NOT_ARRIVED
}
