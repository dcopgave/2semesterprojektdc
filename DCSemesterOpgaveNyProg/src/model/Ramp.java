package model;

import model.enumClasses.OrderStatus;
import model.enumClasses.Transportmateriel;
import model.enumClasses.TruckStatus;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.NoSuchElementException;
/**
 * Created by H&J Group on 25-03-2015.
 */
public class Ramp {
    private String id;
    private boolean isOperational, isStarting;
    private LinkedList<LoadingOrder> queue = new LinkedList<>();
    private Transportmateriel transportmateriel;
    private ErrorQueue errorQueue = ErrorQueue.getInstance();

    public Ramp(String id, Transportmateriel transportmateriel) {
        this.id = id;
        this.transportmateriel = transportmateriel;
        this.isOperational = true;
        this.isStarting = false;
    }

    /**
     * Returns the loadingorder the methods has prepared for loading.
     * @param loadingOrder (the first loadingorder in the queue)
     * @return Loadingorder with actualstart to now, orderstatus ongoing and truckstatus loading
     */
    private LoadingOrder prepareLoadingOrder(LoadingOrder loadingOrder){
        loadingOrder.setActualStart(LocalTime.now());
        loadingOrder.setStatus(OrderStatus.ONGOING);
        loadingOrder.getTruck().setStatus(TruckStatus.LOADING);
        return loadingOrder;
    }

    /**
     * The method removes the first loadingorder from the queue after is has been prepared.
     * @return The Loadingorder that has been removed from the queue.
     */
    public LoadingOrder executeOrder(){
        this.prepareLoadingOrder(getQueue().getFirst());
        LoadingOrder nextLoadingorder = queue.removeFirst();
        nextLoadingorder.getTruck().setStatus(TruckStatus.LOADED);
        return nextLoadingorder;
    }

    /**
     * The methods check whether or not there is a loadingorder in the errorQueue that the ramp has to execute.
     * If the errorQueue is empty it will take the first loadingorder in the queue and execute that,
     * else it will chech whether or not there is a loadingorde on the errorQueue that the ramp has to take and
     * then execute that loadingorder.
     * @return the nextLoadingorder in the Queue.
     */
    public LoadingOrder getNextLoadingOrder() {
        LoadingOrder nextLoadingOrder = null;
        if (errorQueue.isEmpty()){
            nextLoadingOrder = this.executeOrder();
        } else{
           /* if (this.queue.size() == 1){
                nextLoadingOrder = this.executeOrder();
            }*/
            if (errorQueue.hasErrorLoadingOrderToRamp(this)) {
                nextLoadingOrder = errorQueue.getNextErroredLoadingOrder(this.transportmateriel, this);
            } else {
                nextLoadingOrder = this.executeOrder();
            }
        }
        return nextLoadingOrder;
    }

    public LinkedList<LoadingOrder> getQueue(){
        return this.queue;
    }

    /**
     * update the ramps queue so it has the nextErrored LoadingOrder if one exist.
     */
    public void updateQueue(){
        if (errorQueue.hasErrorLoadingOrderToRamp(this)){
            errorQueue.getNextErroredLoadingOrder(this.getTransportmateriel(), this);
        }
    }

    /**
     * adds a loadingOrder to the ramps queue system.
     * @param loadingOrder
     */
    public void addToQueue(LoadingOrder loadingOrder) {
        this.queue.add(loadingOrder);
    }

    /**
     * adds a loadingOrder to the ramps queue system with a specific position.
     * @param position
     * @param loadingOrder
     */
    public void addToQueueWithPosition(int position, LoadingOrder loadingOrder) {
        this.queue.add(position, loadingOrder);
    }

    /**
     * The method return the endtime of the last LoadingOrder in the queue system.
     * @return the end time of the last Loadingorder in the queue.
     * @throws NoSuchElementException
     */
    public LocalTime getExpectedEndFromLast() throws NoSuchElementException{
        if (queue.size() > 0) {
            return queue.getLast().getExpectedEnd();
        } else {
            return LocalTime.now();
        }
    }

    public boolean isOperational() {
        return isOperational;
    }

    /**
     * The method sets the boolean isOperational for the ramp.
     * If the ramps is not operational is will remove all of the loadingorders in the queue and add it to the
     * errorQueue.
     * if the ramps is operational it will take the next loadingorder by a call the the function getNextLoadingOrder()
     * @param boolean isOperational
     */
    public void setOperational(boolean isOperational) {

        if(isOperational == false) {
            ArrayList<LoadingOrder> toRemove = new ArrayList<>();
            for(LoadingOrder l : getQueue()) {
                l.setStatus(OrderStatus.ERROR);
                toRemove.add(l);
            }
            for(LoadingOrder l : toRemove) {
                getQueue().remove(l);
                l.setRamp(null);
            }
        } else{
            if (!errorQueue.isEmpty())
                this.getNextLoadingOrder();
        }
        this.isOperational = isOperational;
    }

    /**
     * takes the first Loadingorder in the queue and removes it.
     * after it sets the orderstatus to Error and sets both actualEnd and start to null.
     * it also sets the starting boolean to falls because no loadingorder is started at the ramp now.
     */
    public void setErrorStateOnFirstOrder(){
        LoadingOrder loadingOrder = this.getQueue().removeFirst();
        loadingOrder.setStatus(OrderStatus.ERROR);
        loadingOrder.setActualStart(null);
        loadingOrder.setActualEnd(null);
        loadingOrder.setRamp(null);
        this.setStarting(false);
    }




    public Transportmateriel getTransportmateriel() {
        return transportmateriel;
    }

    public void setTransportmateriel(Transportmateriel transportmateriel) {
        this.transportmateriel = transportmateriel;
    }

    public String getId() {
        return id;
    }

    public void removeFirst(){
        queue.removeFirst();
    }

    public boolean isStarting() {
        return isStarting;
    }

    public void setStarting(boolean isStarting) {
        this.isStarting = isStarting;
    }

    @Override
    public String toString() {
        return "Rampe nr: " + id + " Transportmateriel: " + getTransportmateriel() + " Operationel: " + isOperational;
    }
}
