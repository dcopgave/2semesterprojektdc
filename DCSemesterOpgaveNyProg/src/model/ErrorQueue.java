package model;

import model.enumClasses.Transportmateriel;

import java.util.ArrayList;

/**
 * Created by H&J Group on 25-03-2015.
 */
public class ErrorQueue {
    private ArrayList<LoadingOrder> queue = new ArrayList<>();
    private static ErrorQueue singleton;

    private ErrorQueue(){

    }

    /**
     *
     * @return the unige errorQueue.
     * it is this method that makes the errorQueue singleton.
     */
    public static ErrorQueue getInstance(){
        if (singleton == null){
            singleton = new ErrorQueue();
        }
        return singleton;
    }

    /**
     *
     * @param ramp
     * @return true if the errorQueue
     */
    public boolean hasErrorLoadingOrderToRamp(Ramp ramp){
        for (LoadingOrder l : this.queue){
            if (l.getTruck().getTransportmateriel().equals(ramp.getTransportmateriel())){
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param transportmateriel
     * @param ramp
     * @return the next Loadingorder for the errorQueue that matches the specific transportmateriel, and adds it the the ramps queue.
     */
    public LoadingOrder getNextErroredLoadingOrder(Transportmateriel transportmateriel, Ramp ramp){
        for(LoadingOrder l : this.queue) {
            if(l.getTruck().getTransportmateriel().equals(transportmateriel)) {
                this.queue.remove(l);
                l.setRamp(ramp);
                ramp.addToQueueWithPosition(0,l);
                return l;
            }
        }
        return null;
    }

    public ArrayList<LoadingOrder> getQueue() {
        return new ArrayList<>(queue);
    }

    public void addToQueue(LoadingOrder loadingOrder){
        this.queue.add(loadingOrder);
    }

    /**
     *
     * @return true if the queue is empty and falls if it's not.
     */
    public boolean isEmpty(){
        if (this.queue.size() > 0){
            return false;
        } else{
            return true;
        }
    }
}
