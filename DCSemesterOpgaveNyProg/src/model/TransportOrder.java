package model;

import model.enumClasses.Transportmateriel;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by H&J Group on 25-03-2015.
 */
public class TransportOrder {
    private String id;
    private double grossWeight;
    private double weightMargin;
    private ArrayList<TransportPartialOrder> partialOrders = new ArrayList<TransportPartialOrder>();
    private Transportmateriel transportmateriel;

    public TransportOrder(String id, double grossWeight, double weightMargin, Transportmateriel transportmateriel) {
        this.id = id;
        this.grossWeight = grossWeight;
        this.weightMargin = weightMargin;
        this.transportmateriel = transportmateriel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(double grossWeight) {
        this.grossWeight = grossWeight;
    }

    public double getWeightMargin() {
        return weightMargin;
    }

    public void setWeightMargin(double weightMargin) {
        this.weightMargin = weightMargin;
    }
    /**
     * Creates a partialOrder and adds it to the TransportOrders list for transportpartialorders.
     * @param double weight
     * @param Localdate loadingDate
     * @param Duration loadingDuration
     * @param String id
     * @param Truck truck
     * @return
     */
    public TransportPartialOrder createTransportPartialOrder(double weight, LocalDate loadingDate,
                                                             Duration loadingDuration, String id, Truck truck){
        TransportPartialOrder tpo = new TransportPartialOrder(weight, loadingDate, loadingDuration, id, truck, this);
        partialOrders.add(tpo);
        return tpo;
    }

    public ArrayList<TransportPartialOrder> getPartialOrders(){
        return new ArrayList<TransportPartialOrder>(partialOrders);
    }

    public void RemovePartialOrder(TransportPartialOrder transportPartialOrder){
        partialOrders.remove(transportPartialOrder);
    }

    public Transportmateriel getTransportmateriel() {
        return transportmateriel;
    }

    public void setTransportmateriel(Transportmateriel transportmateriel) {
        this.transportmateriel = transportmateriel;
    }

    @Override
    public String toString() {
        return "Transportorder: " +
                id  + "\n \t" +
                "Bruttovægt: " + grossWeight + "\n \t" +
                "Vægtmargin: " + weightMargin + "\n \t" +
                "Transportmateriel: " + getTransportmaterielToString() ;
    }
    /**
     * the method changes the languages if the material to danish
     * @return
     */
    public String getTransportmaterielToString(){
        if (getTransportmateriel().equals(Transportmateriel.CHRISTMASTREE)){
            return "Juletræ";
        } else if (getTransportmateriel().equals(Transportmateriel.BOX)){
            return "Kasse";
        } else return "Kar";
    }
}
