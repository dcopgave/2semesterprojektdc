package gui;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.LoadingOrder;
/**
 * Created by H&J Group on 25-03-2015.
 */
public class ArrivalDialog extends Stage {
    private TextArea textArea = new TextArea();
    private LoadingOrder loadingOrder;
    private Button btnOk = new Button("OK!");

    public ArrivalDialog(LoadingOrder loadingOrder){
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.loadingOrder = loadingOrder;

        this.setTitle("Dine læsseinformationer");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
        btnOk.setOnAction(event -> okAction());
    }

    public void initContent(GridPane pane){
        pane.add(textArea, 0, 0);
        pane.add(btnOk, 0,1);
        textArea.setEditable(false);
        textArea.setText(loadingOrder.toString());
    }

    public void okAction(){
        this.hide();
    }
}
