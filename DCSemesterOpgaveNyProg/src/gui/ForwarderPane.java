package gui;

import dao.Storage;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.TransportOrder;
import model.enumClasses.Transportmateriel;
import service.Service;

import java.awt.event.ActionEvent;
import java.time.LocalTime;
/**
 * Created by H&J Group on 25-03-2015.
 */
public class ForwarderPane extends BorderPane {
    private TextField txID = new TextField();
    private TextField txGrossWeight = new TextField();
    private TextField txWeightMargin = new TextField();
    private ComboBox<Transportmateriel> comboBox = new ComboBox<Transportmateriel>();
    private TextField txLoginName = new TextField();
    private PasswordField txLoginPassword = new PasswordField();
    private Button btnLogin = new Button("Log ind");
    private Label lbTitle = new Label("Speditør login");
    private Boolean isLoggedIn = false;
    private GridPane loginGrid = new GridPane();
    private GridPane loggedInGrid = new GridPane();
    private Button btCreate = new Button("Opret!");
    private ListView<TransportOrder> transportOrderListView = new ListView<>();
    private Button btCreatePartialOrder = new Button("Opret delordre");
    private Label lbError = new Label();

    public ForwarderPane(){
        loggedInGrid.setAlignment(Pos.CENTER);
        this.setPadding(new javafx.geometry.Insets(50));

        loggedInGrid.setHgap(20);
        loggedInGrid.setVgap(20);
        loggedInGrid.setGridLinesVisible(false);

        loggedInGrid.add(txID, 0, 0);
        txID.setPromptText("Transportorder ID");
        loggedInGrid.add(txGrossWeight, 1, 0);
        txGrossWeight.setPromptText("Ordrens totale vægt");
        loggedInGrid.add(txWeightMargin, 0, 1);
        loggedInGrid.add(comboBox, 1, 1);
        comboBox.getItems().addAll(Transportmateriel.BOX,Transportmateriel.CHRISTMASTREE, Transportmateriel.TUB);
        txWeightMargin.setPromptText("Vægt margin");
        GridPane buttonGrid = new GridPane();
        loggedInGrid.add(buttonGrid, 0, 2,2,1);
        buttonGrid.setAlignment(Pos.CENTER);
        buttonGrid.add(btCreate, 0, 0);
        buttonGrid.add(btCreatePartialOrder, 1,0);
        loggedInGrid.add(transportOrderListView, 3,0,1,3);
        transportOrderListView.setMaxHeight(150);
        transportOrderListView.setMinWidth(350);
        transportOrderListView.getItems().addAll(Service.getInstance().getTransportOrders());
        btCreate.setOnAction(event -> createAction());
        btCreatePartialOrder.setOnAction(event ->createPartialAction());
        buttonGrid.setHgap(10);

        GridPane title = new GridPane();
        this.setTop(title);
        title.add(lbTitle, 0, 0);
        lbTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 40));
        title.setAlignment(Pos.CENTER);

        loginGrid.setVgap(10);
        loginGrid.setHgap(20);
        loginGrid.setAlignment(Pos.CENTER);
        loginGrid.add(txLoginName, 0, 0);
        loginGrid.add(txLoginPassword, 1, 0);
        GridPane btnGrid = new GridPane();
        btnGrid.add(btnLogin, 0, 0);
        loginGrid.add(btnGrid, 0, 1, 2, 1);
        btnGrid.setAlignment(Pos.CENTER);
        txLoginName.setPromptText("Brugernavn");
        txLoginPassword.setPromptText("Kodeord");

        gridAction();
        btnLogin.setOnAction(event -> loginAction());
        ChangeListener changeListener = (ov, oov, old) -> listViewAction();
        transportOrderListView.getSelectionModel().selectedItemProperty().addListener(changeListener);
        btCreatePartialOrder.setDisable(true);

        this.setBottom(lbError);
    }

    public void gridAction(){
        if(!isLoggedIn) {
            this.setCenter(loginGrid);
        } else {
            this.setCenter(loggedInGrid);
        }
    }

    public void loginAction() {
        if(txLoginName.getText().equals("admin") && txLoginPassword.getText().equals("admin")){
            isLoggedIn = true;
            gridAction();
        }
    }

    public void updateControls() {
        isLoggedIn = false;
        txLoginPassword.clear();
        txLoginName.clear();
        gridAction();
        transportOrderListView.getItems().clear();
        transportOrderListView.getItems().addAll(Service.getInstance().getTransportOrders());
    }

    public void createAction(){
        Service.getInstance().createTransportOrder(txID.getText(), Double.parseDouble(txGrossWeight.getText()),
                Double.parseDouble(txWeightMargin.getText()), comboBox.getSelectionModel().getSelectedItem());
        transportOrderListView.getItems().clear();
        transportOrderListView.getItems().addAll(Service.getInstance().getTransportOrders());
        transportOrderListView.getSelectionModel().selectLast();
    }

    public void listViewAction(){
        if (transportOrderListView.getSelectionModel().getSelectedItem() != null){
            btCreatePartialOrder.setDisable(false);
        } else {
            btCreatePartialOrder.setDisable(true);
        }
    }

    public void createPartialAction() {
        if (transportOrderListView.getSelectionModel().getSelectedItem() == null) {
            lbError.setTextFill(Color.RED);
            lbError.setText("Vælg den transportordre du vil oprette din delordre ud fra");
            btCreatePartialOrder.setDisable(true);
        } else {
            lbError.setText("");
            btCreatePartialOrder.setDisable(false);
            TransportOrder transportOrder = (TransportOrder)transportOrderListView.
                    getSelectionModel().getSelectedItem();
            ForwarderDialog forwarderDialog = new ForwarderDialog(transportOrder);
            forwarderDialog.showAndWait();
            transportOrderListView.getItems().clear();
            transportOrderListView.getItems().addAll(Service.getInstance().getTransportOrders());
        }
    }
}
