package gui;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.*;
import model.enumClasses.OrderStatus;
import service.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by H&J Group on 25-03-2015.
 */
public class RampPane extends BorderPane {
    private ListView lwLoadingOrders = new ListView();
    private ComboBox comboBox = new ComboBox();
    private Button btnNext = new Button("Næste ordre");
    private ToggleButton btnError = new ToggleButton("Fejlmeld rampe!");
    private Label lbTitle = new Label("Rampens ordrer");
    private boolean isStarting = false;
    private boolean errorNext = true;
    private Button btnErrorLoadingordre = new Button("Fejlmeld loadingordre!");

    public RampPane(){
        GridPane centerGrid = new GridPane();
        this.setCenter(centerGrid);
        centerGrid.setAlignment(Pos.CENTER);

        centerGrid.add(comboBox, 0, 0);
        centerGrid.add(lwLoadingOrders, 0, 1);
        centerGrid.setHgap(20);
        centerGrid.setVgap(20);
        centerGrid.setGridLinesVisible(false);

        comboBox.getItems().addAll(Service.getInstance().getRamps());
        ChangeListener changeListener = (ov, oov, old) ->
                updateListView((Ramp) comboBox.getSelectionModel().getSelectedItem());


        GridPane titleGrid = new GridPane();
        this.setTop(titleGrid);
        lbTitle.setPadding(new javafx.geometry.Insets(30));
        titleGrid.add(lbTitle, 0, 0);
        lbTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 40));
        titleGrid.setAlignment(Pos.CENTER);

        comboBox.valueProperty().addListener(changeListener);
        btnNext.setOnAction(event -> nextAction());
        btnError.setOnAction(event -> errorAction());
        GridPane btnGrid = new GridPane();
        centerGrid.add(btnGrid, 0, 2);
        btnGrid.setAlignment(Pos.CENTER);
        btnGrid.add(btnNext, 0, 0);
        btnGrid.add(btnError, 1, 0);
        btnGrid.setHgap(10);
        btnError.setDisable(true);

        titleGrid.setVgap(20);
        GridPane bottomgap = new GridPane();
        Label label1 = new Label("");
        this.setBottom(bottomgap);
        bottomgap.add(label1, 0, 0);
        lwLoadingOrders.setPlaceholder(new Label("Der er ingen loadingordre at vise"));
        btnGrid.add(btnErrorLoadingordre, 2, 0);
        btnNext.setDisable(true);
        btnErrorLoadingordre.setDisable(true);
        btnErrorLoadingordre.setOnAction(event -> errorFirstAction());
    }

    /**
     * The method is calls when the toggleButton is called.
     * it will call the setErrorStateOnFirstOrder() on the ramp if one loadingorder is started and after it will always
     * remove the entire queue and adds it to the errorQueue.
     *
     * If the and is not operational when the method is called that it will the the isOperatinal boolean on the ramp to
     * true.
     */
    public void errorAction() {
        Ramp ramp = this.getRamp();
        if (ramp.isOperational()){
            lwLoadingOrders.setPlaceholder(new Label("Rampen er i fejltilstand"));
            if (ramp.isStarting()){
                if (ramp.getQueue().size() != 0) {
                    ramp.setErrorStateOnFirstOrder();
                }
            }
            ramp.setOperational(false);
        } else {
            ramp.setOperational(true);
            lwLoadingOrders.setPlaceholder(new Label("Der er ingen loadingordre at vise"));
        }
        updateListView(ramp);
        updateButtonState(ramp);
    }

    public boolean isErrorNext() {
        return errorNext;
    }

    public void setErrorNext(boolean errorNext) {
        this.errorNext = errorNext;
    }

    /**
     * The methods calls the setErrorStateOnFirstOrder on the ramps and updates the listview and the buttons.
     */
    public void errorFirstAction(){
        Ramp ramp = this.getRamp();
        ramp.setErrorStateOnFirstOrder();
        setErrorNext(false);
        updateListView(ramp);
        updateButtonState(ramp);
    }

    public Ramp getRamp(){
        return (Ramp) comboBox.getSelectionModel().getSelectedItem();
    }

    /**
     * if one ramps is selected it will update the listview.
     * @param ramp
     */
    public void updateListView(Ramp ramp) {
        lwLoadingOrders.getItems().clear();
        if (ramp != null) {
            lwLoadingOrders.getItems().addAll(ramp.getQueue());
            updateButtonState(ramp);
        }

    }

    /**
     * updates to button so that the state of the button are correct.
     * @param ramp
     */
    public void updateButtonState(Ramp ramp){
        boolean value;
        if ((ramp.getQueue().size()  > 0 || ErrorQueue.getInstance().hasErrorLoadingOrderToRamp(ramp))
                && ramp.isOperational()){
            value = false;
        } else {
            value = true;
        }
        btnNext.setDisable(value);
        btnError.setDisable(false);
        btnErrorLoadingordre.setDisable(value);
        if (errorNext == false){
            btnErrorLoadingordre.setDisable(true);
        } else {
            btnErrorLoadingordre.setDisable(false);
        }

        if (ramp.isOperational()){
            btnError.setSelected(false);
        } else {
            btnError.setSelected(true);
        }
    }

    /**
     * sets the actualstart to now on the first order of the ramp.
     * @param ramp
     */
    public void startOrder(Ramp ramp){
        ramp.getQueue().getFirst().setActualStart(LocalTime.now());
    }

    /**
     * sets the actualend to now on the first order of the ramp.
     * and it calls the smsAction() method that will make the popup.
     * @param ramp
     */
    public void endOrder(Ramp ramp){
        ramp.getQueue().getFirst().setActualEnd(LocalTime.now());
        smsAction(ramp.getQueue().getFirst().getTruck());

    }

    /**
     * takes the nextloadingorder in the queue.
     */
    public void nextAction(){
        Ramp ramp = this.getRamp();
        ramp.updateQueue();
        if (ramp.getQueue().size() > 0 && ramp.isStarting()) {
            endOrder(ramp);
            System.out.println(ramp.getQueue().getFirst().getLoadingDate().toString() + " ::" +
                    ramp.getQueue().getFirst().getActualStart() + "  " + ramp.getQueue().getFirst().getActualEnd());
            setErrorNext(true);
            updateButtonState(ramp);
            ramp.getNextLoadingOrder();
        }
        if (ramp.getQueue().size() > 0 && !ramp.isStarting()) {
            RampDialog rampDialog = new RampDialog(ramp);
            rampDialog.showAndWait();
            if (isStarting){
                startOrder(ramp);
            }
            setErrorNext(true);
        }
        if (ramp.getQueue().size() > 0 && ramp.isStarting()){
            startOrder(ramp);
        }
        updateListView(ramp);
    }

    public void updateControls() {
        comboBox.getSelectionModel().select(null);
    }

    /**
     * checks whether or not the truck can drive away when the methods is called.
     * if not it will start a timer that will expire when the driver is allowed to drive again.
     * @param truck
     */
    public void smsAction(Truck truck){
        if (checkIfDepartureIsOK(truck)){
            SmsDialog smsDialog = new SmsDialog(truck);
            smsDialog.show();

        } else {
            Thread t = new Thread();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            SmsDialog smsDialog = new SmsDialog(truck);
                            smsDialog.show();
                        }
                    });
                }
            };
            Timer timer = new Timer(true);
            long time = (long) ChronoUnit.MILLIS.between(LocalTime.now(),
                    truck.getTruckdriver().getEarliestDepartureTime());
            timer.schedule(timerTask, time);
        }
    }

    /**
     * helper methods to smsAction()
     * checks whether or not departure is ok for the driver.
     * @param truck
     * @return
     */
    public boolean checkIfDepartureIsOK(Truck truck){
        if (LocalTime.now().isAfter(truck.getTruckdriver().getEarliestDepartureTime())){
            return true;
        } else {
            return false;
        }
    }

}
