package gui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Ramp;

import java.time.LocalTime;

/**
 * Created by H&J Group on 25-03-2015.
 */
public class RampDialog extends Stage {
    private Button btOK = new Button("Ja");
    private Button btNO = new Button("Nej");
    private Label lbRampID = new Label();

    public RampDialog(Ramp ramp){
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        GridPane gridPane = new GridPane();
        initContent(gridPane, ramp);
        gridPane.setAlignment(Pos.CENTER);
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
    }

    public void okAction(Ramp ramp){
        ramp.setStarting(true);
        this.hide();
    }

    public void noAction(Ramp ramp) {
        ramp.setStarting(false);
        this.hide();
    }

    public void updateControls(boolean isStarting){
        isStarting = false;
    }
    public void initContent(GridPane pane, Ramp ramp){
        pane.add(btOK, 0 ,1);
        pane.add(btNO, 1,1);
        pane.setVgap(10);
        pane.setHgap(10);
        pane.setAlignment(Pos.CENTER);

        btOK.setOnAction(event -> okAction(ramp));
        btNO.setOnAction(event -> noAction(ramp));
        GridPane lblGrid = new GridPane();
        lblGrid.add(lbRampID, 0, 0);
        lblGrid.setAlignment(Pos.CENTER);


        lbRampID.setText("Du er ved at start næste læsning på rampe: " + ramp.getId() + ". \nØnsker du at forsætte?");
        pane.add(lblGrid, 0, 0, 2, 1);
    }
}
