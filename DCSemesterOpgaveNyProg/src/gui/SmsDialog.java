package gui;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Truck;

/**
 * Created by mathiashemmsen on 02/04/15.
 */
public class SmsDialog extends Stage {
    private Label lbTo = new Label("Til");
    private Button btnOk = new Button("OK!");
    private TextArea textArea = new TextArea();
    private TextField textField = new TextField();
    private Label lbMessage = new Label("Besked");
    public SmsDialog(Truck truck){
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        GridPane pane = new GridPane();
        this.initContent(pane, truck);

        this.setTitle("sms");
        Scene scene = new Scene(pane);
        this.setScene(scene);



    }
    public void initContent(GridPane pane, Truck truck){
        pane.add(lbTo, 0,0);
        pane.add(textField, 1,0);
        pane.add(lbMessage, 0,1);
        pane.add(textArea, 1, 1);
        pane.add(btnOk, 1, 2);
        btnOk.setOnAction(event -> okAction());
        pane.setAlignment(Pos.CENTER);

        textField.setText(truck.getTruckdriver().getPhonenumber());
        textArea.setText("Hej " + truck.getTruckdriver().getName() + "\n" +
        "Din trailer er klar til afhentning. Du har fået læsset " + truck.getTotalLoadingWeight() + " kg. " + "Hav en god dag. \n Venlig hilsen Danish Crown");
        lbTo.setMinWidth(30);
        pane.setVgap(10);
        pane.setHgap(10);
        textArea.setEditable(false);
        textField.setEditable(false);
    }

    public void okAction(){
        this.close();
    }
}


