package gui;
/**
 * Created by H&J Group on 25-03-2015.
 */

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.LoadingOrder;
import model.TransportPartialOrder;
import model.Truck;
import model.Truckdriver;
import model.enumClasses.OrderStatus;
import model.enumClasses.TruckStatus;
import service.Service;

import java.time.Duration;

public class ArrivalPane extends BorderPane {
    private TextField txfTruckId, txfDriverName,txfPhoneNo, txfRestTime;
    private ListView<TransportPartialOrder> lwPartialorders;
    private Button btnAnkomst;
    private Label lblError;

    public ArrivalPane() {
        GridPane gridPane = new GridPane();
        this.setCenter(gridPane);
        gridPane.setAlignment(Pos.CENTER);
        this.setPadding(new javafx.geometry.Insets(50));

        gridPane.setHgap(20);
        gridPane.setVgap(20);
        gridPane.setGridLinesVisible(false);

        GridPane velkomstGrid = new GridPane();
        this.setTop(velkomstGrid);
        Label velkomst = new Label("Velkommen til Danish Crown Horsens");
        velkomstGrid.add(velkomst, 0, 0);
        velkomst.setFont(Font.font("Verdana", FontWeight.BOLD, 40));
        velkomstGrid.setAlignment(Pos.CENTER);

        txfTruckId = new TextField();
        gridPane.add(txfTruckId, 0, 1, 1, 1);
        txfTruckId.setPromptText("Lastbilens ID");
        txfTruckId.setMinHeight(40);
        txfTruckId.setMinWidth(300);

        txfDriverName = new TextField();
        gridPane.add(txfDriverName, 0, 2, 1, 1);
        txfDriverName.setPromptText("Dit navn");
        txfDriverName.setMinHeight(40);


        txfPhoneNo = new TextField();
        gridPane.add(txfPhoneNo, 0, 3, 1, 1);
        txfPhoneNo.setPromptText("Dit telefonnummer");
        txfPhoneNo.setMinHeight(40);


        txfRestTime = new TextField();
        gridPane.add(txfRestTime, 0, 4, 1, 1);
        txfRestTime.setPromptText("Din hviletid i timer");
        txfRestTime.setMinHeight(40);


        lwPartialorders = new ListView<>();
        gridPane.add(lwPartialorders, 1, 1, 1, 4);
        lwPartialorders.setMaxHeight(220);
        lwPartialorders.setMinWidth(400);
        lwPartialorders.setPlaceholder(new Label("Her får du vist dine transportdelordre når du taster lastbilens id"));

        lblError = new Label();
        lblError.setTextFill(Color.RED);
        gridPane.add(lblError, 1, 5);

        btnAnkomst = new Button("Jeg er Ankommet!");
        btnAnkomst.setDisable(true);
        VBox button = new VBox(btnAnkomst);
        this.setBottom(button);
        button.setAlignment(Pos.CENTER);
        btnAnkomst.setAlignment(Pos.CENTER);
        btnAnkomst.setOnAction(event -> arrivalAction());
        txfTruckId.textProperty().addListener((ov, oldCompny, newCompany) -> this.todaysTransportPartialOrders());
    }

    /**
     * clears all the textfields and the listview when you returns from an other tab
     */
    public void updateControls(){
        lwPartialorders.getItems().clear();
        txfRestTime.setText("");
        txfTruckId.setText("");
        txfPhoneNo.setText("");
        txfDriverName.setText("");
    }

    /**
     * gets all the transportpartialorders of the day for the truck and displays it in the listview.
     */
    public void todaysTransportPartialOrders() {
        lwPartialorders.getItems().clear();
        if (hasOnGoingLoadingOrder()){
            lblError.setText("Lastbilens loadingorder er oprettet");
            btnAnkomst.setDisable(true);
        } else {
            try {
                lwPartialorders.getItems().clear();
                lwPartialorders.getItems().addAll(Service.getInstance().getTruckFromID(txfTruckId.getText())
                        .getTodaysPartialOrders());
                lblError.setText("");
                btnAnkomst.setDisable(false);
            } catch (NullPointerException n) {
                if (txfTruckId.getText().length() > 2) {
                    lblError.setText("Lastbilen findes ikke i systemet");
                    btnAnkomst.setDisable(true);
                } else {
                    btnAnkomst.setDisable(false);
                    lblError.setText("");
                }
            }
        }
    }

    /**
     * if the phonenumber is valid, the resttime and the driver has a valid name. It will set the truck to arrived
     * and create a loadingorder
     */
    public void arrivalAction() {
        Truck truck = Service.getInstance().getTruckFromID(txfTruckId.getText());
            int restTime = 0;
            if (isValidPhoneNo(txfPhoneNo.getText()) && isValidResttime(txfRestTime.getText())
                    && isValidName(txfDriverName.getText())) {
                lblError.setText("");
                restTime = Integer.parseInt(txfRestTime.getText());
                truck.setTruckdriver(new Truckdriver(txfDriverName.getText(), truck));
                truck.getTruckdriver().setAdditionalInformation(txfPhoneNo.getText(), Duration.ofHours(restTime));
                truck.setStatus(TruckStatus.ARRIVED);
                LoadingOrder loadingOrder = Service.getInstance().createLoadingOrder(truck.getId());
                ArrivalDialog arrivalDialog = new ArrivalDialog(loadingOrder);
                arrivalDialog.showAndWait();
                txfTruckId.clear();
                txfDriverName.clear();
                txfPhoneNo.clear();
                txfRestTime.clear();
            } else {
                lblError.setText("De indtastede informationer er forkerte");
            }
    }

    /**
     *
     * @param string
     * @return true if the string has 8 numbers in it else it returns falls.
     */
    public boolean isValidPhoneNo(String string){
        if (string.matches("\\d{8}")){
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param string
     * @return true if the resttime is under 25 hours else returns falls.
     */
    public boolean isValidResttime(String string) {
        if(string.matches("\\d{1,2}") && Integer.parseInt(string) < 25) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param string
     * @return true if the name is longer that 1 character and the string does not hold any numbers.
     */
    public boolean isValidName(String string) {
        if(string.length() > 1 && string.matches("\\D+")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return true if there already exist a loadingorder that is not done.
     */
    public boolean hasOnGoingLoadingOrder() {
        boolean hasOngoing = false;
        Truck truck = Service.getInstance().getTruckFromID(txfTruckId.getText());
        try {
            for (LoadingOrder l : truck.getLoadingOrders()) {
                if (!l.getStatus().equals(OrderStatus.DONE)) {
                    hasOngoing = true;
                }
            }
        }catch (NullPointerException n){
        }
        return hasOngoing;
    }


}
