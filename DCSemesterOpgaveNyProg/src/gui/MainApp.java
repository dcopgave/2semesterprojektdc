package gui;

/**
 * Created by H&J Group on 25-03-2015.
 */

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import service.Service;

public class MainApp extends Application {

    public static void main(String[] args)
    {
        Application.launch(args);
    }

    @Override
    public void init()
    {
        Service.getInstance().createSomeObjects();
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Danish Crown Horsens");
        BorderPane pane = new BorderPane();
        this.initContent(pane);
        stage.setResizable(true);
        stage.setHeight(stage.getHeight());
        stage.setWidth(stage.getWidth());
        stage.setMinHeight(600);
        stage.setMinWidth(1024);
        stage.setFullScreen(false);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Platform.exit();
                System.exit(0);
            }
        });
    }

    private void initContent(BorderPane pane){
        TabPane tabPane = new TabPane();
        this.initTabPane(tabPane);
        pane.setCenter(tabPane);
    }

    private void initTabPane(TabPane tabPane) {
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

        Tab tabArrival = new Tab("Ankomst");
        tabPane.getTabs().add(tabArrival);
        Tab tabWeight = new Tab("Vejning");
        tabPane.getTabs().add(tabWeight);
        Tab tabRamp = new Tab("Rampe");
        tabPane.getTabs().add(tabRamp);
        Tab tabForwarder = new Tab("Speditør");
        tabPane.getTabs().add(tabForwarder);


        ArrivalPane arrivalPane = new ArrivalPane();
        tabArrival.setContent(arrivalPane);
        tabArrival.setOnSelectionChanged(event -> arrivalPane.updateControls());

        WeightPane weightPane = new WeightPane();
        tabWeight.setContent(weightPane);
        tabWeight.setOnSelectionChanged(event -> weightPane.updateControls());

        RampPane rampPane = new RampPane();
        tabRamp.setContent(rampPane);
        tabRamp.setOnSelectionChanged(event -> rampPane.updateControls());


        ForwarderPane forwarderPane = new ForwarderPane();
        tabForwarder.setContent(forwarderPane);
        tabForwarder.setOnSelectionChanged(event -> forwarderPane.updateControls());





    }
}
