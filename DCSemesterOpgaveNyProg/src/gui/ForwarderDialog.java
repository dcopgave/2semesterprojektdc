package gui;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import model.TransportOrder;
import model.TransportPartialOrder;
import service.Service;

import java.time.Duration;


/**
 * Created by mathiashemmsen on 31/03/15.
 */
public class ForwarderDialog extends Stage {
    private TextField txWeight = new TextField();
    private DatePicker DPLoadingDate = new DatePicker();
    private TextField txLoadingDuration = new TextField();
    private TextField txID = new TextField();
    private TextField txTruck = new TextField();
    private ListView<TransportPartialOrder> lwPartialOrders = new ListView<>();
    private Button btCreate = new Button("Opret!");
    private Label lbTitle = new Label("Her tastes delordren");
    private TextField txName = new TextField();
    private Label lbError = new Label();

    public ForwarderDialog(TransportOrder transportOrder){
        BorderPane borderPane = new BorderPane();
        GridPane gridPane = new GridPane();
        borderPane.setCenter(gridPane);
        Scene scene = new Scene(borderPane);
        this.setScene(scene);

        gridPane.add(txID, 0, 1);
        txID.setPromptText("Delordrens ID");
        gridPane.add(txTruck, 1, 1);
        gridPane.add(txLoadingDuration, 0, 2);
        txLoadingDuration.setPromptText("Delordrens læsningstid");
        txTruck.setPromptText("Lastbilens ID");
        gridPane.add(txWeight, 1, 2);
        txWeight.setPromptText("Delordrens vægt");
        gridPane.add(DPLoadingDate, 0, 3);
        DPLoadingDate.setPromptText("Delordrens dato");
        gridPane.add(txName, 1,3);



        GridPane btGrid = new GridPane();
        gridPane.add(btGrid, 0, 4, 2, 1);
        btGrid.add(btCreate ,0 ,0);
        btGrid.setAlignment(Pos.CENTER);
        gridPane.add(lwPartialOrders, 2, 0,2, 4);
        lwPartialOrders.setMaxHeight(150);
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setMinWidth(850);
        gridPane.setMinHeight(400);
        gridPane.setAlignment(Pos.CENTER);

        ChangeListener changeListener = ((ov, old, news) -> changeListener());
        txTruck.textProperty().addListener(changeListener);
        txID.textProperty().addListener(changeListener);
        txLoadingDuration.textProperty().addListener(changeListener);
        txWeight.textProperty().addListener(changeListener);
        txName.textProperty().addListener(changeListener);
        DPLoadingDate.valueProperty().addListener(changeListener);
        btCreate.setDisable(true);
        txName.setPromptText("Chaufførens navn");

        GridPane gridPane1 = new GridPane();
        borderPane.setBottom(gridPane1);
        gridPane1.add(lbError, 0,0 );
        gridPane1.setAlignment(Pos.CENTER);

        lbTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 40));
        GridPane titleGrid = new GridPane();
        borderPane.setTop(titleGrid);
        titleGrid.setAlignment(Pos.BOTTOM_CENTER);
        titleGrid.add(lbTitle, 0, 0);
        lbTitle.setAlignment(Pos.BOTTOM_CENTER);
        titleGrid.setVgap(10);
        lbTitle.setPadding(new Insets(50,50,10,10));

        lwPartialOrders.getItems().addAll(Service.getInstance().getTransportPartialOrders(transportOrder));
        btCreate.setOnAction(event -> createAction(transportOrder));

    }

    public void createAction(TransportOrder transportOrder){
            if (txWeight.getText().length() > 0 && DPLoadingDate.valueProperty().getValue() != null &&
                    txLoadingDuration.getText().length() > 0 && txID.getText().length() > 0 &&
                    txTruck.getText().length() > 0) {
                btCreate.setDisable(false);
                transportOrder.createTransportPartialOrder(Double.parseDouble(txWeight.getText()),
                        DPLoadingDate.getValue(), Duration.ofMinutes(Long.parseLong(txLoadingDuration.getText())),
                        txID.getText(), Service.getInstance().getTruckWithID(txTruck.getText(), txName.getText()));
                lwPartialOrders.getItems().clear();
                lwPartialOrders.getItems().addAll(Service.getInstance().getTransportPartialOrders(transportOrder));
            } else {
                btCreate.setDisable(true);
            }
    }

    public void changeListener(){
        if (txWeight.getText().length() > 0 && DPLoadingDate.valueProperty().getValue() != null &&
                txLoadingDuration.getText().length() > 0 && txID.getText().length() > 0 &&
                txTruck.getText().length() > 0 ){
            btCreate.setDisable(false);
            lbError.setText("Vær opmærksom på at hvis ikke du har tastet en lastbilchauffør" +
                    " bliver den lastbilchauffør lastbilen allerede har i systemet brugt.");
        }
        else {
            btCreate.setDisable(true);
        }
    }
}
