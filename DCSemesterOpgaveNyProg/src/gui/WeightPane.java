package gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.TransportPartialOrder;
import model.Truck;
import model.enumClasses.OrderStatus;
import model.enumClasses.TruckStatus;
import service.Service;

import java.time.LocalDate;
import java.util.ArrayList;


/**
 * Created by H&J Group on 25-03-2015.
 */
public class WeightPane extends BorderPane {
    TextField txWeight = new TextField();
    TextField txTruckID = new TextField();
    Label lbError = new Label();
    Button btOK = new Button("OK!");
    Button btError = new Button("Fejl!");
    Label lbTitle = new Label("Indtast vægten på lastbilen");
    TextArea txaInfo = new TextArea();

    /**
     * This pane sets the arrival weight and departure weight depending on the trucs status.
     */
    public WeightPane(){
        GridPane gridPane = new GridPane();
        this.setCenter(gridPane);
        gridPane.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(50));

        gridPane.setHgap(20);
        gridPane.setVgap(20);
        gridPane.setGridLinesVisible(false);
        GridPane title = new GridPane();
        this.setTop(title);
        title.add(lbTitle, 0, 0);
        lbTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 40));
        title.setAlignment(Pos.CENTER);

        gridPane.add(txTruckID,0 ,0);
        gridPane.add(txWeight, 0, 1);
        txTruckID.setPromptText("Lastbilens ID");


        txWeight.setPromptText("Lastbilens vægt");
        GridPane okGrid = new GridPane();
        gridPane.add(okGrid, 0, 2);
        okGrid.setHgap(10);
        okGrid.add(btOK, 0,0);
        okGrid.add(btError, 1, 0);
        btOK.setOnAction(event -> okAction());
        btError.setOnAction(event -> errorAction());
        btOK.setDisable(true);
        btError.setDisable(true);
        okGrid.setAlignment(Pos.CENTER);
        gridPane.add(txaInfo, 1, 0, 1, 3);
        txaInfo.setMaxHeight(125);
        txaInfo.setMaxWidth(300);
        txaInfo.setEditable(false);
        HBox hBox = new HBox();
        hBox.getChildren().addAll(lbError);
        gridPane.add(hBox, 0,3);

        txWeight.textProperty().addListener((ov, old, news) -> this.textAreaAction());
        txTruckID.textProperty().addListener((ov, old, news) -> this.truckIDAction());
    }

    public void setErroredPartialOdersOndone(ArrayList<TransportPartialOrder> transportPartialOrders){
        for (TransportPartialOrder t: transportPartialOrders){
            if (t.getLoadingDate().equals(LocalDate.now())){
               t.setDone(false);
            }
        }
    }

    public void errorAction() {
        Truck truck = Service.getInstance().getTruckFromID(txTruckID.getText());
        truck.getTodaysLoadingOrder().setStatus(OrderStatus.ERROR);
        setErroredPartialOdersOndone(truck.getPartialOrders());
        btOK.setDisable(true);
        lbError.setText("Din Lastbil skal omlastes");
    }

    public void okAction(){
        Truck truck = Service.getInstance().getTruckFromID(txTruckID.getText());
        if (truck.getStatus().equals(TruckStatus.ARRIVED)){
            try {
                lbError.setText("");
                truck.setArrivalWeight(Integer.parseInt(txWeight.getText()));
                txWeight.clear();
                txTruckID.clear();
            } catch (NumberFormatException n){
                lbError.setText("Vægten skal indtastes som et tal");
            }
        } else if (truck.getStatus().equals(TruckStatus.LOADED)) {
            if (truck.checkDepartureWeight(Double.parseDouble(txWeight.getText())) == true) {
                truck.setDepartureWeight(Integer.parseInt(txWeight.getText()));
                truck.setStatus(TruckStatus.DEPARTURED);
                txTruckID.clear();
                txWeight.clear();
            } else {
                lbError.setText("Check venligst at du har indtastet vægten korrekt. \n" +
                        " Klik på fejlknappen for omlæsning.");
            }
        }
    }

    public void truckIDAction(){
        Truck truck = Service.getInstance().getTruckFromID(txTruckID.getText());
        if (truck == null){
            btError.setDisable(true);
            btOK.setDisable(true);
        } else if ((truck.getStatus().equals(TruckStatus.ARRIVED) || truck.getStatus().equals(TruckStatus.LOADED)) &&
                txWeight.getText().length() > 0){
            btError.setDisable(false);
            btOK.setDisable(false);
        }
    }
    public void textAreaAction(){
        Truck truck = Service.getInstance().getTruckFromID(txTruckID.getText());
        if (txWeight.getText().length() < 1){
            txaInfo.setText("Indtast venligst informationerne i felterne");
        } else {
            truckIDAction();
            if (truck != null) {
                if (truck.getStatus().equals(TruckStatus.ARRIVED)) {
                    txaInfo.clear();
                    txaInfo.setText("Lastens forventede vægt: " + truck.getTotalLoadingWeight() + " kg \n" +
                            "Lastbilens forventede vægt: " + (truck.getTotalLoadingWeight() +
                            Integer.parseInt(txWeight.getText())) + " kg");
                } else {
                    txaInfo.clear();
                    txaInfo.setText("Lastbilens forventede vægt: " + (truck.getTotalLoadingWeight() +
                            truck.getArrivalWeight()) + " kg \n" +
                            "Lastbilens faktiske vægt: " + txWeight.getText() + "\n" +
                            "Lastens faktiske vægt: " + (Integer.parseInt(txWeight.getText()) -
                            truck.getArrivalWeight() ) + "\n" +
                            "Vægtdifferencen på lasten: " + (Integer.parseInt(txWeight.getText()) -
                            (truck.getTotalLoadingWeight() + truck.getArrivalWeight()) + " kg"));
                }
            } else {
                txaInfo.clear();
                txaInfo.setText("Du skal indtaste et gyldigt lastbil ID");
            }
        }
    }


    public void updateControls() {
        lbError.setText("");
        txWeight.setText("");
        txTruckID.setText("");
        txaInfo.setText("");
    }

}
