package service;

import dao.Storage;
import model.*;
import model.enumClasses.OrderStatus;
import model.enumClasses.Transportmateriel;
import model.enumClasses.TruckStatus;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

/**
 * Created by H&J Group on 25-03-2015.
 */
public class Service {
    private static Service uniqueInstance;

    private Service(){};

    public static Service getInstance(){
        if (uniqueInstance == null){
            uniqueInstance = new Service();
        }
        return uniqueInstance;
    }

    public Truck getTruckWithID(String id, String name){
        Truck truck1 = getTruckFromID(id);
        if (truck1 == null){
            truck1 = Service.getInstance().createTruck(id, name);
        }
        return truck1;
    }

    public Truck getTruckFromID(String id){
        Truck truck1 = null;
        for (int i = 0; i < Storage.getTrucks().size(); i++){
            if (Storage.getTrucks().get(i).getId().equals(id)){
                truck1 = Storage.getTrucks().get(i);
            }
        }
        return truck1;
    }

    public Object[] getBestRampWithPosition(Duration duration, ArrayList<Ramp> ramps){
        Object[] resultset = new Object[3];
        LocalTime bestTime = LocalTime.of(23,59);
        for (Ramp r : ramps){
            for (int i = 0; i < r.getQueue().size() - 1; i++){
                Duration duration1 = Duration.ofMinutes(ChronoUnit.MINUTES.between(r.getQueue().get(i).getExpectedEnd(),
                        r.getQueue().get(i + 1).getTruck().getTruckdriver().getEarliestDepartureTime()));
                if (!duration1.minus(duration).isNegative() || duration1.minus(duration).isZero()){
                    if (r.getQueue().get(i).getExpectedEnd().isBefore(bestTime)){
                        resultset[0] = r;
                        resultset[1] = i+1;
                        resultset[2] = r.getQueue().get(i).getExpectedEnd();
                    }
                }
            }
        }
        return resultset;
    }

    public LoadingOrder createLoadingOrder(String truckID) throws IllegalArgumentException{
        Truck truck = getTruckFromID(truckID);
        ArrayList<Ramp> ramps = new ArrayList<>();
        Object[] rampAtPosition = new Object[3];
        LoadingOrder finalLoadingOrder = null;
        Transportmateriel transportmateriel = null;
        if (truck != null){
            LoadingOrder loadingOrder = new LoadingOrder(truck);
            LocalTime expectedEnd = truck.getTruckdriver().getEarliestDepartureTime();
            loadingOrder.setExpectedEnd(expectedEnd);
            loadingOrder.setExpectedStart(expectedEnd.minusMinutes(truck.getTotalLoadingTime().toMinutes()));
            if (truck.getTodaysPartialOrders().size() > 0) {
                transportmateriel = truck.getTodaysPartialOrders().get(0).getTransportOrder().getTransportmateriel();
            }
            if (transportmateriel != null){
                ramps.addAll(getOperationalRampswithSpecificMateriel(transportmateriel));
                rampAtPosition = getBestRampWithPosition(truck.getTotalLoadingTime(), ramps);
            }
            Ramp ramp = (Ramp)rampAtPosition[0];
            if (ramp == null){
                LocalTime bestLoadingTime = LocalTime.of(23, 59);
                for (Ramp r : ramps){
                    if (r.getExpectedEndFromLast().isBefore(bestLoadingTime)){
                        bestLoadingTime = r.getExpectedEndFromLast();
                        ramp = r;
                    }
                }
                loadingOrder.setExpectedStart(bestLoadingTime);
                loadingOrder.setExpectedEnd(bestLoadingTime.plusMinutes(truck.getTotalLoadingTime().toMinutes()));
                ramp.addToQueue(loadingOrder);
                truck.addLoadingOrder(loadingOrder);
                loadingOrder.setRamp(ramp);
                return loadingOrder;
            } else{
                if (rampAtPosition[1] instanceof Integer) {
                    Integer position = (Integer) rampAtPosition[1];
                    ramp.addToQueueWithPosition(position, loadingOrder);
                    if (rampAtPosition[2] instanceof LocalTime) {
                        LocalTime startTime = (LocalTime) rampAtPosition[2];
                        loadingOrder.setExpectedStart(startTime);
                        loadingOrder.setExpectedEnd(startTime.plusMinutes(truck.getTotalLoadingTime().toMinutes()));
                    }
                }
            }
            loadingOrder.setRamp(ramp);
            truck.addLoadingOrder(loadingOrder);
            finalLoadingOrder = loadingOrder;
        }
        return finalLoadingOrder;
    }

    public ArrayList<Ramp> getOperationalRampswithSpecificMateriel(Transportmateriel transportmateriel){
        ArrayList<Ramp> resultList = new ArrayList<>();
        for (Ramp r : Storage.getRamps()){
            if (r.getTransportmateriel().equals(transportmateriel) && r.isOperational()){
                resultList.add(r);
            }
        }
        return new ArrayList<>(resultList);
    }

    public Truck createTruck(String id, String name){
        Truck truck = new Truck(id, name);
        Storage.addTruck(truck);
        return truck;
    }

    public TransportOrder createTransportOrder(String id, double grossWeight, double weightMargin,
                                               Transportmateriel transportmateriel){
        TransportOrder transportOrder = new TransportOrder(id, grossWeight, weightMargin, transportmateriel);
        Storage.addTransportOder(transportOrder);
        return transportOrder;
    }

    public TransportPartialOrder serviceCreateTransportPartialOrder(TransportOrder transportOrder, double weight,
                                                                    LocalDate loadingdate, Duration loadingduration,
                                                                    String id, Truck truck){
        return transportOrder.createTransportPartialOrder(weight, loadingdate, loadingduration, id, truck);
    }

    public Ramp createRamp(String id, Transportmateriel transportmateriel){
        Ramp ramp = new Ramp(id, transportmateriel);
        Storage.addRamp(ramp);
        return ramp;
    }

    public void createSomeObjects(){


        createSomeRamps();
        makeTransportOrders();

        for (Truck t : Storage.getTrucks()){
            t.setStatus(TruckStatus.ARRIVED);
            t.getTruckdriver().setAdditionalInformation("11111111", Duration.ofSeconds(5));
            Service.getInstance().createLoadingOrder(t.getId());
        }

        TransportOrder transportOrder = createTransportOrder("1", 1000, 10, Transportmateriel.CHRISTMASTREE);
        serviceCreateTransportPartialOrder(transportOrder, 600, LocalDate.now(), Duration.ofMinutes(30), "1.1",
                getTruckWithID("111", "Poul"));
        serviceCreateTransportPartialOrder(transportOrder, 400, LocalDate.now(), Duration.ofMinutes(20), "1.2",
                getTruckWithID("112", "Henrik"));
        findTruckDriverFromTruck("111").setAdditionalInformation("12345678", Duration.ofHours(11));
        findTruckDriverFromTruck("112").setAdditionalInformation("23456789", Duration.ofHours(9));

        TransportOrder transportOrder1 = createTransportOrder("2", 2000, 8, Transportmateriel.CHRISTMASTREE);
        serviceCreateTransportPartialOrder(transportOrder1, 1300, LocalDate.now(), Duration.ofMinutes(45), "2.1",
                getTruckWithID("111", "Poul"));
        serviceCreateTransportPartialOrder(transportOrder1, 700, LocalDate.now(), Duration.ofMinutes(35), "2.2",
                getTruckWithID("112", "Henrik"));



    }

    public Truckdriver findTruckDriverFromTruck(String truckId){
        Truckdriver truckdriver = null;
        for (Truck t : Storage.getTrucks()){
            if (t.getId().equals(truckId)){
                truckdriver = t.getTruckdriver();
            }
        }
        return truckdriver;
    }

    public ArrayList<Ramp> getRamps(){
        return Storage.getRamps();
    }

    public ArrayList<TransportOrder> getTransportOrders(){
        return Storage.getTransportOrders();
    }

    public ArrayList<TransportPartialOrder> getTransportPartialOrders(TransportOrder transportOrder){
        return transportOrder.getPartialOrders();
    }

    public ArrayList<TransportOrder> makeTransportOrders(){
        ArrayList<TransportOrder> resultList = new ArrayList<>();
        for (int i = 0; i < 11; i++){
            int weights = 0;
            TransportOrder transportOrder = Service.getInstance().createTransportOrder(i +"",
                    0 ,10, Transportmateriel.BOX);
            resultList.add(transportOrder);
            for (int j = 0; j < 21; j++){
                weights += ((int)((Math.random() * 4 + 7)*50));
                transportOrder.createTransportPartialOrder((int)((Math.random() * 4 + 7)*50), LocalDate.now(),
                        Duration.ofMinutes(5) , i + "." + j, Service.getInstance().getTruckWithID(j+"", "Jepser" + j) );
            }
            transportOrder.setGrossWeight(weights);
        }

        for (int i = 11; i < 21; i++){
            int weights = 0;
            TransportOrder transportOrder = Service.getInstance().createTransportOrder(i +"",
                    0, 10, Transportmateriel.TUB);
            resultList.add(transportOrder);
            for (int j = 21; j < 42; j++){
                weights += ((int)((Math.random() * 4 + 7)*50));
                transportOrder.createTransportPartialOrder((int)((Math.random() * 4 + 7)*50), LocalDate.now(),
                        Duration.ofMinutes(6) , i + "." + j, Service.getInstance().getTruckWithID(j+"", "Jan" + j) );
            }
            transportOrder.setGrossWeight(weights);
        }

        for (int i = 21; i < 31; i++){
            int weights = 0;
            TransportOrder transportOrder = Service.getInstance().createTransportOrder(i +"",
                    0, 10, Transportmateriel.CHRISTMASTREE);
            resultList.add(transportOrder);
            for (int j = 42; j < 63; j++){
                weights += ((int)((Math.random() * 4 + 7)*50));
                transportOrder.createTransportPartialOrder((int)((Math.random() * 4 + 7)*50), LocalDate.now(),
                        Duration.ofMinutes(7) , i + "." + j, Service.getInstance().getTruckWithID(j+"", "Birgit" + j) );
            }
            transportOrder.setGrossWeight(weights);
        }
        System.out.println(resultList);
        return resultList;
    }

    public void createSomeRamps(){
        for (int i = 0; i < 7; i++){
            Service.getInstance().createRamp(i + "", Transportmateriel.BOX);
        }

        for (int i = 7; i < 15; i++){
            Service.getInstance().createRamp(i + "", Transportmateriel.CHRISTMASTREE);
        }

        for (int i = 15; i < 22; i++){
            Service.getInstance().createRamp(i + "", Transportmateriel.TUB);
        }
    }

}
