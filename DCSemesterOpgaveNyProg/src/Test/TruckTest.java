package Test;

import model.LoadingOrder;
import model.TransportOrder;
import model.TransportPartialOrder;
import model.Truck;
import model.enumClasses.Transportmateriel;
import org.junit.Before;
import org.junit.Test;
import service.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class TruckTest {
    private TransportOrder transportOrder;
    private TransportOrder transportOrder1;
    private Truck truck;
    private Truck truck1;
    private ArrayList<TransportPartialOrder> transportPartialOrders;
    private ArrayList<TransportPartialOrder> transportPartialOrders1;
    private LoadingOrder loadingOrder;
    private LoadingOrder loadingOrder1;
    private TransportPartialOrder transportPartialOrder;
    private TransportPartialOrder transportPartialOrder1;

    @Before
    public void Setup(){
        transportOrder = Service.getInstance().createTransportOrder("1", 2000, 10, Transportmateriel.BOX);
        transportOrder1 = Service.getInstance().createTransportOrder("2", 1000, 10, Transportmateriel.CHRISTMASTREE);
        truck = Service.getInstance().createTruck("001", "Per");
        truck1 = Service.getInstance().createTruck("002", "Henrik");
        transportPartialOrders = new ArrayList<>();
        transportPartialOrders1 = new ArrayList<>();
        transportPartialOrder = transportOrder.createTransportPartialOrder(600, LocalDate.now(),
                Duration.ofMinutes(30), "1.2", truck);
        transportPartialOrder1 = transportOrder.createTransportPartialOrder(600, LocalDate.now(),
                Duration.ofMinutes(20), "1.3", truck);
        transportPartialOrders.add(transportPartialOrder);
        transportPartialOrders.add(transportPartialOrder1);
        transportPartialOrders1.add(transportOrder1.createTransportPartialOrder(800, LocalDate.now(),
                Duration.ofMinutes(40), "2.1", truck1));
        truck.getTruckdriver().setAdditionalInformation("12345678", Duration.ofMinutes(60));
        truck1.getTruckdriver().setAdditionalInformation("11223344", Duration.ofMinutes(120));
        Service.getInstance().createSomeRamps();
        loadingOrder = Service.getInstance().createLoadingOrder("001");
        loadingOrder1 = Service.getInstance().createLoadingOrder("001");
    }

    @Test
    public void testGetPartialOrders(){
        assertTrue(truck.getPartialOrders().equals(transportPartialOrders));
        assertTrue(truck1.getPartialOrders().equals(transportPartialOrders1));
    }

    @Test
    public void testGetTotalLoadingWeight(){
        assertEquals(1200, truck.getTotalLoadingWeight());
        assertFalse(truck.getTotalLoadingWeight() <= 1199);
        assertFalse(truck.getTotalLoadingWeight() >=1201);
        assertEquals(800, truck1.getTotalLoadingWeight());
        assertFalse(truck1.getTotalLoadingWeight() == 799);
        assertFalse(truck1.getTotalLoadingWeight() == 801);
    }

    @Test
    public void testGetTodaysLoadingOrder(){
        loadingOrder.setExpectedStart(LocalTime.now());
        loadingOrder.setExpectedEnd(LocalTime.now());
        loadingOrder.setActualStart(LocalTime.now());
        loadingOrder.setActualEnd(LocalTime.now());

        assertTrue(loadingOrder.equals(truck.getTodaysLoadingOrder()));
        loadingOrder.setLoadingDate(LocalDate.now().plusDays(1));
        assertFalse(loadingOrder.equals(truck.getTodaysLoadingOrder()));
    }

    @Test
    public void testGetTotalLoadingTime(){
        loadingOrder.setExpectedStart(LocalTime.now());
        loadingOrder.setExpectedEnd(LocalTime.now());
        loadingOrder.setActualStart(LocalTime.now());
        loadingOrder.setActualEnd(LocalTime.now());

        assertEquals(Duration.ofMinutes(50), truck.getTotalLoadingTime());
        assertEquals(Duration.ofMinutes(40), truck1.getTotalLoadingTime());
        assertFalse(truck.getTotalLoadingTime() == Duration.ofMinutes(49));
        assertFalse(truck.getTotalLoadingTime() == Duration.ofMinutes(51));
        assertFalse(truck1.getTotalLoadingTime() == Duration.ofMinutes(39));
        assertFalse(truck1.getTotalLoadingTime() == Duration.ofMinutes(41));
    }

    @Test
    public void testCheckDepartureWeight(){
        loadingOrder.setExpectedStart(LocalTime.now());
        loadingOrder.setExpectedEnd(LocalTime.now());
        loadingOrder.setActualStart(LocalTime.now());
        loadingOrder.setActualEnd(LocalTime.now());

        truck.setArrivalWeight(1000);

        assertFalse(truck.checkDepartureWeight(2600));
        assertTrue(truck.checkDepartureWeight(1200));
        assertTrue(truck.checkDepartureWeight(1320));
        assertTrue(truck1.checkDepartureWeight(880));
        assertTrue(truck1.checkDepartureWeight(720));
        assertFalse(truck1.checkDepartureWeight(881));
    }

    @Test
    public void testCheckEarliestDepartureTime(){
        assertTrue(truck.getTruckdriver().getEarliestDepartureTime().equals(LocalTime.now().plusMinutes(60)));
        assertTrue(truck1.getTruckdriver().getEarliestDepartureTime().equals(LocalTime.now().plusMinutes(120)));
        assertFalse(truck.getTruckdriver().getEarliestDepartureTime().equals(LocalTime.now().plusMinutes(59)));
        assertFalse(truck.getTruckdriver().getEarliestDepartureTime().equals(LocalTime.now().plusMinutes(61)));
        assertFalse(truck1.getTruckdriver().getEarliestDepartureTime().equals(LocalTime.now().plusMinutes(119)));
        assertFalse(truck1.getTruckdriver().getEarliestDepartureTime().equals(LocalTime.now().plusMinutes(121)));
    }

    @Test
    public void testGetTodaysPartialOrders() throws Exception {
        assertTrue(truck.getPartialOrders().equals(transportPartialOrders));
        assertTrue(truck1.getPartialOrders().equals(transportPartialOrders1));
        assertFalse(truck.getPartialOrders().equals(transportPartialOrders1));
        assertFalse(truck1.getPartialOrders().equals(transportPartialOrders));
    }

    @Test
    public void testGetTransportmateriel() throws Exception {
        assertTrue(truck.getTransportmateriel().equals(Transportmateriel.BOX));
        assertTrue(truck1.getTransportmateriel().equals(Transportmateriel.CHRISTMASTREE));
    }


    @Test
    public void testSetGetArrivalWeight() throws Exception {
        truck.setArrivalWeight(1000);
        assertEquals(1000, truck.getArrivalWeight(), 0.1);
    }


    @Test
    public void testGetId() throws Exception {
        assertTrue(truck.getId().equals("001"));
    }

    @Test
    public void testSetId() throws Exception {
        truck.setId("100");
        assertTrue(truck.getId().equals("100"));
    }


    @Test
    public void testAddPartialOrder() throws Exception {
        TransportPartialOrder transportPartialOrder2 = new TransportPartialOrder(450, LocalDate.now(),
                Duration.ofMinutes(20),"100", truck, transportOrder);
        transportPartialOrders.add(transportPartialOrder2);
        assertTrue(truck.getPartialOrders().equals(transportPartialOrders));
    }

    @Test
    public void testGetLoadingOrders() throws Exception {
        ArrayList<LoadingOrder> loadingOrders = new ArrayList<>();
        loadingOrders.add(loadingOrder);
        loadingOrders.add(loadingOrder1);
        assertTrue(truck.getLoadingOrders().equals(loadingOrders));
    }

    @Test
    public void testAddLoadingOrder() throws Exception {
        LoadingOrder loadingOrder2 = Service.getInstance().createLoadingOrder("002");
        ArrayList<LoadingOrder> loadingOrders2 = new ArrayList<>();
        loadingOrders2.add(loadingOrder2);
        assertTrue(truck1.getLoadingOrders().equals(loadingOrders2));
    }
}