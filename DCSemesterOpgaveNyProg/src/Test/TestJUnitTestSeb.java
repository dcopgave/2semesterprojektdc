package Test;

import dao.Storage;
import model.*;
import model.enumClasses.Transportmateriel;
import model.enumClasses.TruckStatus;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class TestJUnitTest {
    private TransportOrder transportOrder;
    private TransportOrder transportOrder1;
    private Truck truck;
    private Truck truck1;
    private ArrayList<TransportPartialOrder> transportPartialOrders;
    private ArrayList<TransportPartialOrder> transportPartialOrders1;
    private LoadingOrder loadingOrder;
    private LoadingOrder loadingOrder1;
    private TransportPartialOrder transportPartialOrder;
    private TransportPartialOrder transportPartialOrder1;

    @Before
    public void Setup(){
        transportOrder = Service.getInstance().createTransportOrder("1", 2000, 10, Transportmateriel.BOX);
        transportOrder1 = Service.getInstance().createTransportOrder("2", 1000, 10, Transportmateriel.BOX);
        truck = Service.getInstance().createTruck("001", "Per");
        truck1 = Service.getInstance().createTruck("002", "Henrik");
        transportPartialOrders = new ArrayList<>();
        transportPartialOrders1 = new ArrayList<>();
        transportPartialOrder = transportOrder.createTransportPartialOrder(600, LocalDate.now(), Duration.ofMinutes(30), "1.2", truck);
        transportPartialOrder1 = transportOrder.createTransportPartialOrder(600, LocalDate.now(), Duration.ofMinutes(20), "1.3", truck1);
        transportPartialOrders.add(transportPartialOrder);
        transportPartialOrders1.add(transportPartialOrder1);
        transportPartialOrders.add(transportOrder1.createTransportPartialOrder(800, LocalDate.now(), Duration.ofMinutes(40), "2.1", truck));
        truck.getTruckdriver().setAdditionalInformation("12345678", Duration.ofMinutes(60));
        truck1.getTruckdriver().setAdditionalInformation("11223344", Duration.ofMinutes(120));
        Service.getInstance().createSomeRamps();
        loadingOrder = Service.getInstance().createLoadingOrder("001");
        loadingOrder1 = Service.getInstance().createLoadingOrder("001");


    }

    @Test
    public void testGetPartialOrders(){
        assertTrue(transportPartialOrders.equals(truck.getPartialOrders()));
        assertTrue(transportPartialOrders1.equals(truck1.getPartialOrders()));
    }

    @Test
    public void testGetTotalLoadingWeight(){
        assertTrue(truck.getTotalLoadingWeight() == 1400);
        assertTrue(truck1.getTotalLoadingWeight() == 600);
    }

    @Test
    public void testGetTodaysLoadingOrder(){
        loadingOrder.setExpectedStart(LocalTime.now());
        loadingOrder.setExpectedEnd(LocalTime.now());
        loadingOrder.setActualStart(LocalTime.now());
        loadingOrder.setActualEnd(LocalTime.now());

        assertTrue(loadingOrder.equals(truck.getTodaysLoadingOrder()));
        loadingOrder.setLoadingDate(LocalDate.now().plusDays(1));
        assertFalse(loadingOrder.equals(truck.getTodaysLoadingOrder()));
    }

    @Test
    public void testGetTotalLoadingTime(){
        loadingOrder.setExpectedStart(LocalTime.now());
        loadingOrder.setExpectedEnd(LocalTime.now());
        loadingOrder.setActualStart(LocalTime.now());
        loadingOrder.setActualEnd(LocalTime.now());

        assertEquals(Duration.ofMinutes(70), truck.getTotalLoadingTime());
        assertEquals(Duration.ofMinutes(20), truck1.getTotalLoadingTime());
    }

    @Test
    public void testCheckDepartureWeight(){
        loadingOrder.setExpectedStart(LocalTime.now());
        loadingOrder.setExpectedEnd(LocalTime.now());
        loadingOrder.setActualStart(LocalTime.now());
        loadingOrder.setActualEnd(LocalTime.now());

        truck.setArrivalWeight(1000);

        assertFalse(truck.checkDepartureWeight(2600));
        assertTrue(truck.checkDepartureWeight(2440));
        assertTrue(truck.checkDepartureWeight(2540));

        transportOrder.setWeightMargin(5);
        assertTrue(truck.checkDepartureWeight(2400));
        assertTrue(truck.checkDepartureWeight(2470));
    }

    @Test
    public void testCheckEarliestDepartureTime(){
        assertTrue(truck.getTruckdriver().getEarliestDepartureTime().equals(LocalTime.now().plusMinutes(60)));
    }






}