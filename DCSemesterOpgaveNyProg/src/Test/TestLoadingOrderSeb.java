package Test;

import gui.RampDialog;
import model.*;
import model.enumClasses.OrderStatus;
import model.enumClasses.Transportmateriel;
import model.enumClasses.TruckStatus;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class TestLoadingOrdre {
    private Truck truck, truck1;
    private TransportOrder transportOrder;
    private TransportPartialOrder transportPartialOrder, transportPartialOrder1;
    private LoadingOrder loadingOrder, loadingOrder1;
    private Ramp ramp, rampTub, rampTub1, rampTree;
    private ErrorQueue errorQueue;
    private ArrayList<Ramp> ramps = new ArrayList<>();

    @Before
    public void testSetup() throws Exception {
        this.rampTub = Service.getInstance().createRamp("2", Transportmateriel.TUB);
        this.rampTub1 = Service.getInstance().createRamp("3", Transportmateriel.TUB);
        this.rampTree = Service.getInstance().createRamp("4", Transportmateriel.CHRISTMASTREE);
        this.errorQueue = ErrorQueue.getInstance();
        ramps.add(rampTub);
        ramps.add(rampTub1);
        ramps.add(rampTree);

    }

    @Test
    public void testErroredLoadingOrder() throws Exception{
        Ramp ramp = Service.getInstance().createRamp("1", Transportmateriel.BOX);
        Truck truck = Service.getInstance().createTruck("001", "Hans");
        Truck truck1 = Service.getInstance().createTruck("002", "Per" );

        truck.getTruckdriver().setAdditionalInformation("11223344", Duration.ofMinutes(60));
        truck1.getTruckdriver().setAdditionalInformation("55223344", Duration.ofMinutes(120));

        truck.setStatus(TruckStatus.ARRIVED);
        truck1.setStatus(TruckStatus.ARRIVED);

        TransportOrder transportOrder = Service.getInstance().createTransportOrder("1", 2000, 10, Transportmateriel.BOX);

        TransportPartialOrder transportPartialOrder = transportOrder.createTransportPartialOrder(900, LocalDate.now(), Duration.ofMinutes(25), "1.1", truck);
        TransportPartialOrder transportPartialOrder1 = transportOrder.createTransportPartialOrder(1100, LocalDate.now(), Duration.ofMinutes(25), "1.2", truck1);

        LoadingOrder loadingOrder = Service.getInstance().createLoadingOrder(truck.getId());
        LoadingOrder loadingOrder1 = Service.getInstance().createLoadingOrder(truck1.getId());

        assertTrue(ramp.getQueue().size() == 2);
        assertTrue(ramp.getQueue().getFirst().equals(loadingOrder));

        // Simulate setting error on a loadingorder. Remember to remove it from the queue
        loadingOrder1.setStatus(OrderStatus.ERROR);
        ramp.getQueue().remove(loadingOrder1);

        assertTrue(ramp.getQueue().size() == 1);

        // Next loadingorder is the one added to the errorqueue.
        assertTrue(ramp.getNextLoadingOrder().equals(loadingOrder1));
        loadingOrder1.setStatus(OrderStatus.LOADED);

        assertTrue(ramp.getNextLoadingOrder().equals(loadingOrder));
        loadingOrder.setStatus(OrderStatus.LOADED);

        assertTrue(ramp.getQueue().size() == 0);
    }

    @Test
    public void testQueueSystem() throws Exception {
        Truck truck3 = Service.getInstance().createTruck("3", "Jens");
        truck3.getTruckdriver().setAdditionalInformation("11223344", Duration.ofHours(1));

        Truck truck4 = Service.getInstance().createTruck("4", "Line");
        truck4.getTruckdriver().setAdditionalInformation("55223344", Duration.ofHours(9));

        Truck truck5 = Service.getInstance().createTruck("5", "Brah");
        truck5.getTruckdriver().setAdditionalInformation("55223344", Duration.ofHours(1));

        Truck truck6 = Service.getInstance().createTruck("6", "Bent");
        truck6.getTruckdriver().setAdditionalInformation("55663344", Duration.ofHours(5));

        Truck truck7 = Service.getInstance().createTruck("7", "Boerge");
        truck7.getTruckdriver().setAdditionalInformation("55667744", Duration.ofHours(1));

        // Tub TransportOrder
        TransportOrder tubOrder = Service.getInstance().createTransportOrder("2", 3000, 15, Transportmateriel.TUB);
        TransportPartialOrder tubPartial = tubOrder.createTransportPartialOrder(1000, LocalDate.now(),
                Duration.ofMinutes(30), "2.0", truck3);
        TransportPartialOrder tubPartial2 = tubOrder.createTransportPartialOrder(800, LocalDate.now(),
                Duration.ofMinutes(40), "2.1", truck3);
        TransportPartialOrder tubPartial3 = tubOrder.createTransportPartialOrder(1200, LocalDate.now(),
                Duration.ofMinutes(50), "2.2", truck4);

        // Christmastree TransportOrder
        TransportOrder treeOrder = Service.getInstance().createTransportOrder("3", 3000, 15, Transportmateriel.CHRISTMASTREE);
        TransportPartialOrder treePartial = treeOrder.createTransportPartialOrder(1000, LocalDate.now(),
                Duration.ofMinutes(30), "2.0", truck5);
        TransportPartialOrder treePartial2 = treeOrder.createTransportPartialOrder(800, LocalDate.now(),
                Duration.ofMinutes(30), "2.1", truck6);
        TransportPartialOrder treePartial3 = treeOrder.createTransportPartialOrder(1200, LocalDate.now(),
                Duration.ofMinutes(30), "2.2", truck7);

        // Tub LoadingOrders
        LoadingOrder loadingOrderJens = Service.getInstance().createLoadingOrder(truck3.getId());
        LoadingOrder loadingOrderLine = Service.getInstance().createLoadingOrder(truck4.getId());

        // Chrismastree LoadingOrders
        LoadingOrder loadingOrderBrah = Service.getInstance().createLoadingOrder(truck5.getId());
        LoadingOrder loadingOrderBent = Service.getInstance().createLoadingOrder(truck6.getId());
        LoadingOrder loadingOrderBoerge = Service.getInstance().createLoadingOrder(truck7.getId());


        assertTrue(rampTub.getNextLoadingOrder().equals(loadingOrderJens));
        assertTrue(rampTub1.getNextLoadingOrder().equals(loadingOrderLine));

        assertTrue(rampTree.getQueue().get(0).equals(loadingOrderBrah));
        assertTrue(rampTree.getQueue().get(1).equals(loadingOrderBoerge));
        assertTrue(rampTree.getQueue().get(2).equals(loadingOrderBent));


    }
}